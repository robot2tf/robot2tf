/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worawit.robotproject;

/**
 *
 * @author WIP
 */
public class Fuel extends MyObj{
    int volumn;
    public Fuel(int x, int y, int volumn) {
        super('F', x, y);
        this.volumn = volumn;
    }
    
    public int fillFuel(){
        int vol = volumn;
        symbol = '-';
        volumn = 0;
        return vol;
    }
}
