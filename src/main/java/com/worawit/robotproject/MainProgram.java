/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worawit.robotproject;

import java.util.Scanner;

/**
 *
 * @author werapan
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        TableMap map = new TableMap(20, 20);
        Robot robot = new Robot(2, 2, 'x', map, 10);
        Bomb bomb = new Bomb(5,5);
        map.addObj(new tree(10, 10)); //Priority Low
        map.addObj(new tree(9, 10));
        map.addObj(new tree(10, 9));
        map.addObj(new tree(11, 10));
        map.addObj(new tree(3, 6));
        map.addObj(new tree(3, 5));
        map.addObj(new tree(7, 6));
        map.addObj(new tree(5, 6));
        map.addObj(new tree(4, 4));
        map.addObj(new tree(8, 6));
        map.addObj(new tree(5, 6));
        map.addObj(new Fuel(5, 4, 20));
        map.addObj(new Fuel(0, 3, 20));
        map.addObj(new Fuel(7, 8, 20));

        map.setBomb(bomb);
        map.setRobot(robot);  //Priority High
        
        while(true) {
            map.showMap();
            // W,a| N,w| E,d| S, s| q: quit
            char direction = inputDirection(sc);
            if(direction=='q') {
                printByeBye();
                break;
            }
            robot.walk(direction);
        }
        
    }

    private static void printByeBye() {
        System.out.println("Bye Bye!!!");
    }

    private static char inputDirection(Scanner sc) {
        String str = sc.next();
        return str.charAt(0);
    }
}
